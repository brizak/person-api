package org.testeonix.personapi.restapi.restservice;
import org.testeonix.personapi.restapi.entity.Person;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PersonService {
    
    private final PersonRepository repository;
    
    @Autowired
    public PersonService(PersonRepository repository) {
        this.repository = repository;
    }

    public List<Person> getPersons() {
        return repository.findAll();
    }

    public Optional<Person> getPersonById(UUID id) {
        return repository.findById(id);
    }

    public Iterable<Person> getPersonByKeyword(String keyword) {
        return repository.findPersonBykeyword(keyword);
    }

    public void addNewPerson(Person person) {
        Optional<Person> personOptional = repository.findPersonByName(person.getLastname());
        if(personOptional.isPresent()) {
            throw new IllegalStateException("person exist");
        } 
        repository.save(person);
    }

    public void deletePerson(UUID personId) {
        boolean exists = repository.existsById(personId);
        if(!exists) {
            throw new IllegalStateException("person does not exist");
        } 
        repository.deleteById(personId);
    }

    @Transactional
    public void updatePerson(UUID personId, Person personToUpdate) {
        boolean exists = repository.existsById(personId);
        if(!exists) {
            throw new IllegalStateException("person does not exist");
        } 
        repository.setUserInfoById(personToUpdate.getFirstname(), personToUpdate.getLastname(), personId);
    }
}
