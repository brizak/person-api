package org.testeonix.personapi.restapi.restservice;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.testeonix.personapi.restapi.entity.Person;


@Repository
public interface PersonRepository extends JpaRepository<Person, UUID>  {
    
    @Query(value = "SELECT * FROM person p WHERE p.lastname = ?1 ", nativeQuery = true)
    Optional<Person> findPersonByName(String lastName); 

    @Query(value = "SELECT * FROM person_db.person WHERE firstname LIKE %?1% OR lastname LIKE %?1%", nativeQuery = true)
    Iterable<Person> findPersonBykeyword(String keyword);

    @Modifying
    @Query("update Person p set p.firstname = ?1, p.lastname = ?2 where p.id = ?3")
    void setUserInfoById(String firstname, String lastname, UUID personId);
}
