package org.testeonix.personapi.restapi.controller;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.testeonix.personapi.restapi.entity.Person;
import org.testeonix.personapi.restapi.restservice.PersonRepository;
import org.testeonix.personapi.restapi.restservice.PersonService;

@RestController
@RequestMapping(path = "/api/v1/persons")
public class PersonController {
    
    
    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    public Iterable<Person> findAll() {
        return personService.getPersons();
    }

    @GetMapping(path = "/{personId}")
    public Optional<Person> findById(@PathVariable("personId") UUID id) {
        return personService.getPersonById(id);
    }

    @GetMapping(path = "/keyword/{keyword}")
    public Iterable<Person> findByKeyword(@PathVariable("keyword") String keyword) {
        return personService.getPersonByKeyword(keyword);
    }

    @PostMapping
    public void create(@RequestBody Person person) {
        personService.addNewPerson(person);
    }
    

    @DeleteMapping(path = "/{personId}")
    public void delete(@PathVariable("personId") UUID id) {
        personService.deletePerson(id);
    }

    @PutMapping(path = "/{personId}")
    public void update(@PathVariable("personId") UUID id, @RequestBody Person person) {
        personService.updatePerson(id, person);
    }
}
